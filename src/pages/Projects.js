import { motion } from "framer-motion";
import { useState, useEffect } from "react";
import Pagination from "../components/Pagination";
import ProjectCard from "../components/ProjectCard";

export default function Projects() {
  const [fullStack, setFullStack] = useState([]);
  const [frontEnd, setFrontEnd] = useState([]);
  const [all, setAll] = useState([]);
  const [currentPage, setCurrentPage] = useState(1);
  const [itemPerPage, setItemPerPage] = useState(2);

  const [toggleAll, setToggleAll] = useState(true);
  const [toggleFullStack, setToggleFullStack] = useState(false);
  const [toggleFrontEnd, setToggleFrontEnd] = useState(false);
  const data = [
    {
      title: "Logs",
      stacks: ["MongoDb", "Express", "React", "Node"],
      description: `Blog website built in MERN Stack. Used JWT Authentication for user authentication and bcrypt for password hashing. Styled using React and SASS preprocessor with form validations and error handling. `,
      fullStack: true,
      img: "images/logs.jpeg",
      link: "https://logs-chi.vercel.app/",
    },
    {
      title: "Todo",
      stacks: ["MongoDb", "Express", "React", "Node"],
      description:
        "Todo MERN Stack Application. I used NodeJS, Express and MongoDb Atlas for the backend and React JS for the frontend. It have user verification using jsonwebtoken, simple regex validation for the user inputs in the registration module.",
      fullStack: true,
      img: "images/todo.jpeg",
      link: "https://todo-app-eta-liart.vercel.app/",
    },
    {
      title: "Sneaker Head",
      stacks: ["Grid", "CSS3", "JavaScript"],
      description:
        " E-Commerce landing page using CSS Grid, FlexBox and JavaScript.",
      frontEnd: true,
      img: "images/sh.jpeg",
      link: "https://joarpenaflorida.github.io/LandingPage-Grid/",
    },
    {
      title: "Tenzies",
      stacks: ["React", "Grid"],
      description: "Simple Tenzies game that I made while learning React JS.",
      frontEnd: true,
      img: "images/tenzies.jpeg",
      link: "https://tenzies-game-topaz.vercel.app/",
    },
    {
      title: "Snap",
      stacks: ["Grid", "JavaScript"],
      description:
        "Image Gallery Website using CSS Grid, Flexbox and JavaScript.",
      frontEnd: true,
      img: "images/snap.jpeg",
      link: "https://joarpenaflorida.github.io/image-grid/",
    },
  ];

  const lastPostIndex = currentPage * itemPerPage;
  const firstPostIndex = lastPostIndex - itemPerPage;
  const currentPosts = data.slice(firstPostIndex, lastPostIndex);

  useEffect(() => {
    setAll(
      data.map((data) => {
        return <ProjectCard key={data.title} data={data} />;
      })
    );

    const frontEnd = data.filter((data) => {
      return data.frontEnd === true;
    });
    const fullStack = data.filter((data) => {
      return data.fullStack === true;
    });

    setFullStack(
      fullStack.map((data) => {
        return <ProjectCard key={data.title} data={data} />;
      })
    );
    setFrontEnd(
      frontEnd.map((data) => {
        return <ProjectCard key={data.title} data={data} />;
      })
    );
  }, []);
  const [isDark, setIsDark] = useState(
    localStorage.getItem("theme") === "dark" ? true : false
  );
  useEffect(() => {}, [isDark]);
  const style = {
    fontWeight: "700",
  };
  const standard = {
    fontWeight: "400",
  };

  const toggleShowAll = () => {
    setToggleAll(true);
    setToggleFrontEnd(false);
    setToggleFullStack(false);
  };
  const toggleShowFrontEnd = () => {
    setToggleAll(false);
    setToggleFrontEnd(true);
    setToggleFullStack(false);
  };
  const toggleShowFullStack = () => {
    setToggleAll(false);
    setToggleFrontEnd(false);
    setToggleFullStack(true);
  };

  return (
    <motion.div
      initial={{ opacity: 0, scale: 0.9 }}
      animate={{ opacity: 1, scale: 1 }}
      exit={{ opacity: 0 }}
      transition={{ duration: 1 }}
      className="content"
    >
      <div className="lead-text">
        <p>2 | 3</p>
        <h1>PORTFOLIO</h1>
        <h2>Projects</h2>
      </div>
      <div className="project-title">
        <ul>
          <li style={toggleAll ? style : standard} onClick={toggleShowAll}>
            All
          </li>
          <li
            style={toggleFrontEnd ? style : standard}
            onClick={toggleShowFrontEnd}
          >
            Frontend
          </li>
          <li
            style={toggleFullStack ? style : standard}
            onClick={toggleShowFullStack}
          >
            Fullstack
          </li>
        </ul>
      </div>
      <div className="project-container">
        {toggleAll && all}
        {toggleFullStack && fullStack}
        {toggleFrontEnd && frontEnd}
      </div>
    </motion.div>
  );
}
