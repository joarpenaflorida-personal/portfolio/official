import { motion } from "framer-motion";

export default function About() {
  return (
    <>
      <motion.div
        initial={{ opacity: 0, scale: 0.9 }}
        animate={{ opacity: 1, scale: 1 }}
        exit={{ opacity: 0 }}
        transition={{ duration: 1 }}
        className="content"
      >
        <div className="lead-text">
          <p>1 | 3</p>
          <h1>ABOUT</h1>
          <h2>Shattering the status quo.</h2>
        </div>
        <div className="card-container">
          <motion.div
            whileHover={{
              y: "-2px",
              transition: { duration: 0.5 },
            }}
            className="card"
          >
            <h2>MERN STACK DEVELOPER</h2>
            <p>
              Hi! I'm Joar Penaflorida a Full Stack MERN Developer based in
              Iloilo, Philippines. I develop my software development skills by
              creating mini projects and engaging myself in coding challenges.
            </p>
            <ul>
              <li>
                ● In-depth knowledge of MongoDb, Express JS, React JS, and Node
                JS
              </li>
              <li>● Proficient in Front End and Backend Technologies.</li>
              <li>● Experienced in developing 3-tier API Architecture</li>
            </ul>
          </motion.div>
          <motion.div
            whileHover={{
              y: "-2px",
              transition: { duration: 0.2 },
            }}
            className="card"
          >
            <h2>UX & UI DESIGNER</h2>
            <p>
              Experienced in creating web designs. For the past 3 months, I've
              focus exclusively on product designing through Figma. When it
              comes to UX and UI Design, It's all about fullfilling business
              objectives by balancing visuals with user experiences.
            </p>
            <ul>
              <li>● Optimizing UX and UI experience.</li>
              <li>
                ● Managing software workflow through wireframes, mockups and
                flowcharts.
              </li>
              <li>● Following SEO best practices.</li>
            </ul>
          </motion.div>
        </div>
        <div className="lead-text">
          <h3>Tools</h3>
        </div>
        <div className="tech-stacks">
          <div className="stack">
            <i className="bx bxl-html5" data-value="HTML5"></i>
          </div>
          <div className="stack">
            <i className="bx bxl-css3" data-value="CSS3"></i>
          </div>
          <div className="stack">
            <i className="bx bxl-bootstrap" data-value="Bootstrap"></i>
          </div>

          <div className="stack">
            <i className="bx bxl-tailwind-css" data-value="TailwindCSS"></i>
          </div>
          <div className="stack">
            <i className="bx bxl-sass" data-value="SASS"></i>
          </div>
          <div className="stack">
            <i className="bx bxl-javascript" data-value="JavaScript"></i>
          </div>
          <div className="stack">
            <i className="bx bxl-react" data-value="React JS"></i>
          </div>
          <div className="stack">
            <i className="bx bxl-typescript" data-value="TypeScript"></i>
          </div>
          <div className="stack">
            <i className="bx bxl-nodejs" data-value="Node JS"></i>
          </div>
          <div className="stack">
            <i className="bx bxl-mongodb" data-value="MongoDb"></i>
          </div>
          <div className="stack">
            <i className="bx bxl-figma" data-value="Figma"></i>
          </div>
          <div className="stack">
            <i className="bx bxl-heroku" data-value="heroku"></i>
          </div>
          <div className="stack">
            <i className="bx bxl-git" data-value="Git"></i>
          </div>
        </div>
      </motion.div>
    </>
  );
}
