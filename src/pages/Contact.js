import { motion } from "framer-motion";
import { useEffect, useState } from "react";
import ContactCards from "../components/ContactCards";

export default function Contact() {
  const [contacts, setContacts] = useState([]);
  const data = [
    {
      id: 1,
      icon: "bx bxl-gmail",
      title: "Gmail",
      description: "joarparrenopenaflorida",
      link: "mailto:https://joarparrenopenaflorida@gmail.com",
    },
    {
      id: 2,
      icon: "bx bxl-linkedin",
      title: "LinkedIn",
      description: "in/joarppenaflorida",
      link: "https://linkedin.com/in/joarppenaflorida",
    },
    {
      id: 3,
      icon: "bx bxs-phone",
      title: "Phone",
      description: "+639217320661",
      link: "tel:+639217320661",
    },
  ];
  useEffect(() => {
    setContacts(
      data.map((contact) => {
        return <ContactCards key={contact.id} contact={contact} />;
      })
    );
  }, []);
  return (
    <motion.div
      initial={{ opacity: 0, scale: 0.9 }}
      animate={{ opacity: 1, scale: 1 }}
      exit={{ opacity: 0 }}
      transition={{ duration: 1 }}
      className="content"
    >
      <div className="lead-text">
        <p>3 | 3</p>
        <h1>CONTACT</h1>
        <h2>WANT TO COLLABORATE?</h2>
      </div>
      <div className="card-container">{contacts}</div>
    </motion.div>
  );
}
