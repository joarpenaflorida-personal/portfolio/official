import { Link } from "react-router-dom";
export default function NotFound() {
  return (
    <>
      <div className="notfound">
        <i className="bx bx-code-alt"></i>
        <h1>404</h1>
        <p>Oops... Page not found.</p>
        <Link to="/">Back to About.</Link>
      </div>
    </>
  );
}
