import "./App.css";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import React, { lazy, Suspense } from "react";
// pages

// components
import Navbar from "./components/Navbar";
import Footer from "./components/Footer";

// Framer Motion
import { AnimatePresence, motion } from "framer-motion";

function App() {
  const About = React.lazy(() => import("./pages/About"));
  const Contact = React.lazy(() => import("./pages/Contact"));
  const Projects = React.lazy(() => import("./pages/Projects"));
  const NotFound = React.lazy(() => import("./pages/NotFound"));

  const renderLoader = () => (
    <div className="lds-ripple">
      <div></div>
      <div></div>
    </div>
  );
  return (
    <AnimatePresence>
      <Router>
        <motion.div
          initial={{ opacity: 0 }}
          animate={{ opacity: 1 }}
          exit={{ opacity: 0 }}
          className="container"
        >
          <div className="header">
            <Navbar />
          </div>
          <div className="contents">
            <Routes>
              <Route
                exact
                path="/"
                element={
                  <Suspense fallback={renderLoader()}>
                    <About />
                  </Suspense>
                }
              />

              <Route
                exact
                path="/projects"
                element={
                  <Suspense fallback={renderLoader()}>
                    <Projects />
                  </Suspense>
                }
              />
              <Route
                exact
                path="/contact"
                element={
                  <Suspense fallback={renderLoader()}>
                    <Contact />
                  </Suspense>
                }
              />
              <Route
                path="*"
                element={
                  <Suspense fallback={renderLoader()}>
                    <NotFound />
                  </Suspense>
                }
              />
            </Routes>
          </div>
          <div className="footer">
            <Footer />
          </div>
        </motion.div>
      </Router>
    </AnimatePresence>
  );
}

export default App;
