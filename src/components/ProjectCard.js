import { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import Stacks from "./Stacks";
import { motion } from "framer-motion";
export default function ProjectCard({ data }) {
  const { title, description, stacks, link, fullStack, frontEnd, img } = data;

  const [stack, setStack] = useState([]);

  useEffect(() => {
    setStack(
      stacks.map((stack) => {
        return <Stacks key={stack} data={stack} />;
      })
    );
  }, []);

  return (
    <motion.div
      initial={{ opacity: 0, scale: 0.8 }}
      animate={{ opacity: 1, scale: 1 }}
      exit={{ opacity: 0 }}
      whileHover={{
        y: "-2px",
      }}
      className="card"
    >
      <a href={link} className="tag" target="_blank" rel="noreferrer"></a>
      <img src={img} alt="" />
      <div className="stacks">{stack}</div>
      <h2>{title}</h2>
      <p>{description}</p>
    </motion.div>
  );
}
