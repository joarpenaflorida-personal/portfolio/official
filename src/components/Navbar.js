import { NavLink } from "react-router-dom";
import NavToggle from "./NavToggle";
import { useState } from "react";
import { motion } from "framer-motion";

export default function Navbar() {
  const [active, setIsActive] = useState(false);
  function toggleShow() {
    setIsActive((prevState) => !prevState);
  }

  return (
    <nav>
      <div className="nav--left">
        <div className="logo">
          <h1>JP</h1>
        </div>
      </div>
      <div className="nav--right">
        <ul>
          <li>
            <NavLink to="/">ABOUT</NavLink>
          </li>
          <li>
            <NavLink to="/projects">PROJECTS</NavLink>
          </li>
          <li>
            <NavLink to="/contact">CONTACT</NavLink>
          </li>
          <li>
            <NavToggle />
          </li>
        </ul>
      </div>
      <div className="toggler-responsive">
        <div className="toggle" onClick={toggleShow}>
          {active ? (
            <i className="bx bx-x"></i>
          ) : (
            <i className="bx bx-menu-alt-right"></i>
          )}
        </div>
        {active ? (
          <motion.ul
            initial={{ opacity: 0, y: "-100%" }}
            animate={{ opacity: 1, y: 0 }}
            exit={{ opacity: 0, y: "-100%" }}
            className="toggleShow"
          >
            <li>
              <NavLink to="/" onClick={toggleShow}>
                ABOUT
              </NavLink>
            </li>
            <li>
              <NavLink to="/projects" onClick={toggleShow}>
                PROJECTS
              </NavLink>
            </li>
            <li>
              <NavLink to="/contact" onClick={toggleShow}>
                CONTACT
              </NavLink>
            </li>
          </motion.ul>
        ) : (
          <></>
        )}
      </div>
    </nav>
  );
}
