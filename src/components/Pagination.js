import React from "react";

export default function Pagination({
  totalPosts,
  itemPerPage,
  setCurrentpage,
}) {
  let pages = [];
  for (let i = 1; i <= Math.ceil(totalPosts / itemPerPage); i++) {
    pages.push(i);
  }

  return (
    <div>
      {pages.map((page, index) => {
        return (
          <button key={index} onClick={() => setCurrentpage(page)}>
            {page}
          </button>
        );
      })}
    </div>
  );
}
