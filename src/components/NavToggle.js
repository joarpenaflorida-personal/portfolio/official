import { useState, useEffect } from "react";

export default function NavToggle() {
  const [isDark, setIsDark] = useState(
    localStorage.getItem("theme") === "dark" ? true : false
  );
  useEffect(() => {
    document
      .getElementsByTagName("HTML")[0]
      .setAttribute("data-theme", localStorage.getItem("theme"));
  });

  const themeChanger = () => {
    if (isDark === false) {
      localStorage.setItem("theme", "dark");
      document
        .getElementsByTagName("HTML")[0]
        .setAttribute("data-theme", localStorage.getItem("theme"));
      setIsDark(true);
    } else {
      localStorage.setItem("theme", "light");
      document
        .getElementsByTagName("HTML")[0]
        .setAttribute("data-theme", localStorage.getItem("theme"));
      setIsDark(false);
    }
  };

  return (
    <>
      <div className="toggler" onClick={() => themeChanger()}>
        {isDark ? (
          <i className="bx bxs-sun"></i>
        ) : (
          <i className="bx bxs-moon"></i>
        )}
      </div>
    </>
  );
}
