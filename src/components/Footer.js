export default function Footer() {
  return (
    <>
      <div className="footer-left">
        <ul>
          <li>
            <a
              href="mailTo:joarparrenopenaflorida@gmail.com"
              target="_blank"
              rel="noreferrer"
            >
              <i className="bx bxl-gmail"></i>
            </a>
          </li>
          <li>
            <a
              href="https://linkedin.com/in/joarppenaflorida"
              target="_blank"
              rel="noreferrer"
            >
              <i className="bx bxl-linkedin"></i>
            </a>
          </li>
        </ul>
      </div>
      <div className="footer-right">
        <p>&copy; Joar Penaflorida | 2022</p>
      </div>
    </>
  );
}
