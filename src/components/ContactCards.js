import { motion } from "framer-motion";

export default function ContactCards({ contact }) {
  const { title, icon, description, link } = contact;
  return (
    <motion.div
      whileHover={{
        scale: 1.005,
        transition: { duration: 0.2 },
        y: "-2px",
      }}
      className="card"
    >
      <div className="card-title">
        <h3>{title}</h3>
        <span>
          <i className={icon}></i>
        </span>
      </div>
      <div className="card-description">
        <p>{description}</p>
        <a href={link} target="_blank" rel="noreferrer">
          <i className="bx bx-chevron-right"></i>
        </a>
      </div>
    </motion.div>
  );
}
